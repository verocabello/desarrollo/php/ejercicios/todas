<?php
//indicar salida del código
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 4</title>
    </head>
    <body>
        <?php
        $cadena = "¿Es España la más?";
        var_dump(strtoupper($cadena)); //mal
        var_dump(mb_strtoupper($cadena,"UTF-8")); //bien
        mb_internal_encoding("UTF-8");
        var_dump(strtoupper($cadena)); //mal
        var_dump(mb_strtoupper($cadena)); //bien
        ?>
    </body>
</html>

