<?php
//modificar el código del ejercicio anterior indicando el ámbito de aplicación de cada uno de los miembros, 
//public, private, protected. si hace falta algún miembro static colocarlo
//para clases hijas utilizar parent si existe algún miembro sobreescrito
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 3</title>
    </head>
    <body>
        <?php
        class Animal {
            
            public $name;
            function set_name($text){
                $this->name = $text;
            }
            
            function get_name(){
                return $this->name;
            }
        }
        
        class Leon extends Animal {
            
            public $name;
            function roar(){
                echo "=>", $this->name, " está rugiendo!<br>";
            }
            
            function set_name($text) {
                Animal::set_name($text);
            }
        }
        
        echo "Creando su nuevo león...<br>";
        $leon = new Leon;
        $leon->set_name("Superman");
        $leon->roar();
        ?>
    </body>
</html>

