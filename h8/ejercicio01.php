<?php
//analizar el siguiente código, suponer que se ejecuta 2 veces, por ejemplo actualizando página
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        $handle = fopen("1.txt", "ab");
        
        $text = "\nY\naqui\ntenemos\nmás\ntexto.";
        
        if (fwrite($handle, $text) == FALSE){
            echo 'No';
        } else {
            echo 'Si';
        }
        
        fclose($handle);
        ?>
    </body>
</html>

