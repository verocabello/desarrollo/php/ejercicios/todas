<?php
//indicar las salidas del código
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 8</title>
    </head>
    <body>
        <?php
        
        class Vector{
            
            private $valor;
            private $fragmentos;
            private $longitud;
            
            function __construct($valor = array(1,2,3,4,5,6,7,8,9,)){
                $this->valor = $valor;
                $this->setLongitud();
            }
            
            private function setLongitud(){
                $this->longitud = count($this->valor);
            }
            
            public function rDerecha($posiciones){
                for($c=0; $c<$posiciones; $c++){
                    $a[$c] = array_pop($this->valor);
                }
                $this->valor = array_merge($a,$this->valor);
            }
            
            public function rDerecha1($posiciones){
                //$a = array_reverse(array_slice(array_reverse($this->valor), $posiciones));
                $a = array_slice($this->valor, 0, $this->longitud - $posiciones);
                $b = array_slice($this->valor, $this->longitud - $posiciones); 
                $this->valor = array_merge($b, $a);
            }
            
            public function trocear($elementos){
                $this->fragmentos = array_chunk($this->valor, $elementos);
            }
            
            public function getValor(){
                return $this->valor;
            }
            
            public function getFragmentos(){
                return $this->fragmentos;
            }
            
            public function setValor($valor){
                $this->valor = $valor;
            }
            
            public static function cadenaArray($string){
                $strlen = mb_strlen($string);
                while($strlen){
                    $array[] = mb_substr($string, 0, 1, "UTF-8");
                    $string = mb_substr($string, 1, $strlen, "UTF-8");
                    $strlen = mb_strlen($string);
                }
                return $array;
            }
        }
        
        $objeto = new Vector();
        $objeto->rDerecha(2);
        var_dump($objeto);
        unset($objeto);
        //var_dump($objeto);  produciria error
        $objeto = new Vector();
        $objeto->trocear(5);
        foreach($objeto->getFragmentos() as $indice => $vector){
            $objeto1[$indice] = new Vector($vector);
            $objeto1[$indice]->rDerecha1(1);
        }
        
        var_dump($objeto1);
        
        $cadena = "lá casa de pñpe";
        //$cadena = html_entity_decode($cadena);
        $a = new Vector(explode(" ", $cadena));
        $a->rDerecha1(1);
        var_dump($a->getValor());
        
        foreach ($a->getValor() as $indice => $valor){
            $a1[$indice] = new Vector(Vector::cadenaArray($valor));
            $a1[$indice]->rDerecha1(1);
        }
        $salida = "";
        
        foreach($a1 as $v){
            $salida.=implode($v->getValor());
        }
        
        var_dump($salida);
        
        ?>
    </body>
</html>

