<?php
//indicar la salida del código
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 2</title>
    </head>
    <body>
        <?php
        class Animal {
            
            var $name;
            function set_name($text){
                $this->name = $text;
            }
            
            function get_name(){
                return $this->name;
            }
        }
        
        class Leon extends Animal {
            
            var $name;
            function roar(){
                echo "=>", $this->name, " está rugiendo!<br>";
            }
            
            function set_name($text) {
                Animal::set_name($text);
            }
        }
        
        echo "Creando su nuevo león...<br>";
        $leon = new Leon;
        $leon->set_name("Superman");
        $leon->roar();
        ?>
    </body>
</html>

