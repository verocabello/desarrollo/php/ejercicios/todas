<?php
//realizar una función que le pasas una cadena de caracteres y te crea un array
//varias soluciones
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 7</title>
    </head>
    <body>
        <?php
        $cadena = "¿Es España la más?";
        
        function cadenaArray($string){
            $strlen = strlen($string);
            while($strlen){
                $array[] = substr($string, 0, 1);
                $string = substr($string, 1 , $strlen);
                $strlen = strlen($string);
            }
            return $array;
        }
        
        var_dump(cadenaArray($cadena)); //mal
        
        function cadenaArray2($string){
            $strlen = strlen($string);
            for($c=0; $c<$strlen; $c++){
                $array[$c] = $string[$c];
            }
            return $array;
        }
        
        var_dump(cadenaArray2($cadena)); //mal
        
        function cadenaArray1($string){
            $strlen = mb_strlen($string);
            while ($strlen){
                $array[] = mb_substr($string, 0, 1, "UTF-8");
                $string = mb_substr($string, 1, $strlen, "UTF-8");
                $strlen = mb_strlen($string);
            }
            return $array;
        }
        
        var_dump(cadenaArray1($cadena)); //bien
        
        function cadenaArray3($string){
            $strlen = mb_strlen($string);
            while ($strlen){
                $array[] = mb_substr($string, 0, 1, "UTF-8");
                $string = mb_substr($string, 1, $strlen, "UTF-8");
                $strlen = strlen($string);
            }
            return $array;
        }
        
        var_dump(cadenaArray3($cadena)); //bien
        
        ?>
    </body>
</html>

