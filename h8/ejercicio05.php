<?php
//realizar una clase con un método estático que te permita pasar un string a mayúsculas
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 5</title>
    </head>
    <body>
        <?php
        
        class Cadena {
            
            public static function mayusculas($cadena){
                return mb_strtoupper($cadena, "UTF-8");
            }
        }
        
        var_dump(Cadena::mayusculas("Ejemplo de método"));
        
        //no es necesario
        $v = new Cadena();
        var_dump($v->mayusculas("¿Metodo?"));
        ?>
    </body>
</html>

