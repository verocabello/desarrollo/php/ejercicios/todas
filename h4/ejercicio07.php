<?php
//corregir la siguiente clase para poder realizar la sobrecarga del método constructor utilizando funciones con
//numero de argumentos variables
class Vehiculo{
    
    public $matricula;
    private $color;
    protected $encendido;
    
    function __construct($matricula, $color, $encendido){
        $this -> matricula = $matricula;
        $this -> color = $color;
        $this -> encendido = $encendido;
    }
    
    function encendidoApagado($matricula, $color){
        $this -> matricula = $matricula;
        $this -> color = $color;
        $this -> encendido = false;
    }
    
    function colorBlanco($matricula){
        $this -> matricula = $matricula;
        $this -> color = "blanco";
        $this -> encendido = false;
    }
    
    public function encender(){
        $this -> encendido = true;
        echo 'Vehiculo encendido <br*>';
    }
    
    public function apagar(){
        $this -> encendido = false;
        echo 'Vehículo apagado <br/>';
    }
}

$coche = new Vehiculo("DHH2323", "rojo", false);
$coche -> encender();
var_dump($coche);

echo $coche ->encendidoApagado("RFF4444", "azul");
var_dump($coche);

echo $coche ->colorBlanco("SCC5555");
var_dump($coche);

$coche->apagar();
var_dump($coche);
?>
