<?php
//indicar la salida del codigo
class Usuario{
    var $nombre = "defecto";
    private $edad;
    protected $telefono;
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getEdad(){
        return $this -> edad;
    }
    
    public function getTelefono(){
        return $this -> telefono;
    }
    
    public function setNombre($nombre){
        $this -> nombre = $nombre;
    }
    
    public function setEdad($edad){
        $this -> edad = $edad;
    }
    
    public function setTelefono($telefono){
        $this -> telefono = $telefono;
    }
}

$persona = new Usuario();
echo $persona -> nombre;
$persona -> setEdad(51);
$persona ->setTelefono("606123456");
var_dump($persona);
$persona -> nombre = "Maria";
$persona -> edad = 12;
$persona -> telefono = "654000222";
var_dump($persona);

//muestra todo lo que le pides menos la edad, linea 39, ya que es un elemento privado de la clase
//y el telefono es protected
?>

