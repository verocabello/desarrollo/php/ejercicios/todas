<?php
//indicar la salida del codigo
include "coche.php"
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 4</title>
    </head>
    <body>
        <?php
        $coche =new Coche(); //instanciamos la clase coche
        $coche -> color = 'Rojo'; //llenamos algunas de las propiedades
        $coche -> marca = 'Honda';
        $coche -> numero_puertas = 4;
        $coche -> llenarTanque(10); //utilizamos los metodos
        $coche -> acelerar();
        $coche -> acelerar();
        $coche -> acelerar();
        var_dump($coche);
        ?>
    </body>
</html>

