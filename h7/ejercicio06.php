<?php
//funcion
//crear una función que le paso n argumentos y me devuelve la suma de todos
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 6</title>
    </head>
    <body>
        <?php
        function sumaTodos($array){
            $suma = array_sum($array);
            return $suma;
        }
        
        $numeros = [4,5,8];
        $salida = sumaTodos($numeros);
        echo 'La suma de todos los números dados es <b>' . $salida . '</b>.<br/>';
        
        
        function sumaTodos2(){
            $numeros2 = [4,5,8,10];
            $suma = 0;
            foreach ($numeros2 as $numero) {
                $suma += $numero;
            }
            
            echo 'La suma de todos los números dados es <b>' . $suma . '</b>.';
        }
//        $arraySalida = [2,1,1,2];
        $salida2 = sumaTodos2();
        echo $salida2;
//        echo 'La suma de todos los números dados es <b>' . $salida2 . '</b>.';
        
        
        ?>
    </body>
</html>
