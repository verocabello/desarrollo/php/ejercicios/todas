<?php
//variables
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 3</title>
    </head>
    <body>
        <?php
        echo '<div>Toda la información sobre las variables</div>';
        //variable entera
        $entera = 4;
        echo '<div>La variable $entera vale <b>' . $entera . '</b> y es de tipo <b>' . gettype($entera) . '</b></div>';
        //variable boolean
        $boolean = true;
        echo '<div>La variable $boolean vale <b>' . $boolean . '</b> y es de tipo <b>' . gettype($boolean) . '</b></div>';
        //variable string
        $string = "Hola!";
        echo '<div>La variable $string vale <b>' . $string . '</b> y es de tipo <b>' . gettype($string) . '</b></div>';
        
        //variable array
        $array = [2,3,4,5];
        echo '<div>La variable $array vale <b>' . $array[0] . ', ' . $array[1] . ', ' . $array[2] . ', ' . $array[3] . '</b> y es de tipo <b>' . gettype($array) . '</b></div>';
        ?>
    </body>
</html>
