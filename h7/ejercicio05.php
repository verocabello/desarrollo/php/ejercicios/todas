<?php
//funcion
//crear una función que le paso dos argumentos y me devuelve la suma
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 5</title>
    </head>
    <body>
        <?php
        function sumaDos($num1,$num2){
            $suma = $num1 + $num2;
            return $suma;
        }
        
        $salida = sumaDos(4,5);
        echo 'La suma de los dos números dados es <b>' . $salida . '</b>.';
        
        ?>
    </body>
</html>
