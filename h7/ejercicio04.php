<?php
//variables
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 4</title>
    </head>
    <body>
        <?php
        //variable string
        $variable1 = "Normal";
        //variable string con formato heredoc
        $variable2 = <<<EOT
                Esto es un texto en formato
                de varias lineas<br>
                Formato heredoc.
EOT;
        //crear otra variable en formato heredoc
        $variable3 = <<<EOT
                Texto de ejemplo en formato
                de varias lineas<br> que metemos
                en la variable3.
EOT;
                
        ?>
        <div>
            <?php
            echo 'Variable 1: <b>' . $variable1 . '</b>.<br/>';
            echo 'Variable 2: <b>' . $variable2 . '</b>.<br/>';
            echo 'Variable 3: <b>' . $variable3 . '</b>.';
            ?>
        </div>
    </body>
</html>
