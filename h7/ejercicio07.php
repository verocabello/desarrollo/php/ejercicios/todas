<?php
//funcion
//crear una función que le paso n argumentos y me devuelve la suma y el producto de todos. utilizar un array
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 7</title>
    </head>
    <body>
        <?php
        function sumaTodos($array){
            $suma = array_sum($array);
            return $suma;
        }
        
        $numeros = [4,5,8];
        $salida = sumaTodos($numeros);
        echo 'La suma de todos los números dados es <b>' . $salida . '</b>.<br/>';
        
        
        function sumaTodos2(){
            $numeros2 = [4,5,8,10];
            $suma = 0;
            foreach ($numeros2 as $numero) {
                $suma += $numero;
            }
            
            echo 'La suma de todos los números dados es <b>' . $suma . '</b>.<br/>';
        }

        $salida2 = sumaTodos2();
        echo $salida2;
        
        function multiplicaTodos($array){
            $suma = array_product($array);
            return $suma;
        }
        
        $numeros = [4,5,8];
        $salida = multiplicaTodos($numeros);
        echo 'El producto de todos los números dados es <b>' . $salida . '</b>.<br/>';
        
        function multiplicaTodos2(){
            $numeros2 = [4,5,8,10];
            $producto = 1;
            foreach ($numeros2 as $numero) {
                $producto *= $numero;
            }
            
            echo 'El producto de todos los números dados es <b>' . $producto . '</b>.';
        }

        $salida2 = multiplicaTodos2();
        echo $salida2;
        ?>
    </body>
</html>
