<?php
//variables
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 2</title>
    </head>
    <body>
        <?php
        $variable = TRUE;
        //devolver el tipo de variable
        echo 'Tipo de variable: ' . gettype($variable) . '<br/>';
        //convertir la variable en entero
        //devolver el valor de la variable
        $entero = (int)$variable;
        echo 'Convertir variable a entero: ' . $entero;
        ?>
    </body>
</html>
