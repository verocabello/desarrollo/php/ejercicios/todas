<?php
//arrays
//crear un array enumerado con 4 números
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 8</title>
    </head>
    <body>
        <?php
        $array = [
            1 => "verde",
            2 => "rojo",
            3 => "negro",
            4 => "azul"
        ];
        var_dump($array);
        ?>
    </body>
</html>
