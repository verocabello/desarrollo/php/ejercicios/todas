<?php
//variables
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        //crear una variable llamada a y colocar el valor 10
        $a = 10;
        //mostrar el tipo de dato de la variable a
        echo 'Tipo de dato de variable a: ' . (gettype($a));
        //eliminar la variable a
        unset($a);
        echo 'Eliminar la variable a: ' . $a;    
        ?>
    </body>
</html>
