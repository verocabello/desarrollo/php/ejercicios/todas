<?php
//arrays
//crear un array asociativo con 7 números, utilizar como índice los días de la semana
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 9</title>
    </head>
    <body>
        <?php
        $array = [
            "lunes" => "uno",
            "martes" => "dos",
            "miercoles" => "tres",
            "jueves" => "cuatro",
            "viernes" => "cinco",
            "sabado" => "seis",
            "domingo" => "siete"
        ];
        var_dump($array);
        ?>
    </body>
</html>
