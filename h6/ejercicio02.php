<!-- modificar el ejercicio anterior para que en caso de que no sea un fichero jpg nos de un aviso -->

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 2</title>
    </head>
    <body>
        <?php
        if(!$_POST){
            ?>
            <form method="POST" enctype="multipart/form-data">
                <input type="file" name="archivo" accept="image/*"/>
                <input type="submit" value="enviar" name="enviar"/>
                <input type="reset" value="borrar" name="borrar"/>
            </form>
            <?php
        } else {
            if($_FILES['archivo']['type'] == 'image/jpeg'){
                move_uploaded_file($_FILES["archivo"]['tmp_name'], ".\\" . $_FILES["archivo"]["name"]);
                echo "El archivo se ha subido correctamente al servidor";
            }else{
                echo "tipo de archivo no soportado";
            }
        }
        ?>
    </body>
</html>

