<!-- realizar un ejercicio en php que nos permita subir un archivo via formulario -->

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        if(!$_POST){
            ?>
        <form method="POST" enctype="multipart/form-data">
            <input type="file" name="archivo"/>
            <input type="submit" value="enviar" name="enviar"/>
            <input type="reset" value="borrar" name="borrar"/>
        </form>
    <?php
    } else {
        move_uploaded_file($_FILES["archivo"]['tmp_name'], ".\\" . $_FILES["archivo"]["name"]);
        echo "El archivo se ha subido correctamente al servidor";
    }
    ?>
    </body>
</html>


