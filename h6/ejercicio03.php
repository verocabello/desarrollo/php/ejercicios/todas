<!-- crear un fichero de texto con los números impares del 1 al 1000 (separados por un intro)
    utilizar fwrite -->

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 3</title>
    </head>
    <body>
        <?php
        $fp = fopen("3.txt","w");
        $intro = " ";
        for($c=1; $c<1000; $c+=2){
            fwrite($fp, $c);
            fwrite($fp, "\n");  
        }
        fclose($fp);
        ?>
    </body>
</html>
