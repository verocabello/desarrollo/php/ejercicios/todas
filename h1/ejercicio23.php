<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 23</title>
    </head>
    <body>
        <?php
        $dia = "hoy";
        switch ($dia):
            case "Lunes";
                echo "Hoy es Lunes";
                break;
            case "Martes";
                echo "Hoy es Martes";
                break;
            case "Miércoles";
                echo "Hoy es Miércoles";
                break;
            case "Jueves";
                echo "Hoy es Jueves";
                break;
            case "Viernes";
                echo "Hoy es Viernes";
                break;
            case "Sábado";
                echo "Hoy es Sábado";
                break;
            case "Domingo";
                echo "Hoy es Domingo";
                break;
            default :
                echo "No sé";            
        endswitch;
        ?>
    </body>
</html>
