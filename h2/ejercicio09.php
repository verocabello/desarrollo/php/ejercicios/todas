<!-- desarrollar el siguiente formulario en una pagina web php -->
<?php
if($_REQUEST){
    $mal=false;
} else {
    $mal=true;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"
        <title></title>
    </head>
    <body>
        <?php
        if($mal){
            ?>
            <form name="f">
                <label for="ciudad">Selecciona la ciudad</label>
                <select name="ciudad" id="ciudad">
                    <optgroup label="Asia">
                        <option name="ciudad" value="1" selected>Delhi</option>
                        <option name="ciudad" value="2">Hong Kong</option>
                        <option name="ciudad" value="3">Mumbai</option>
                        <option name="ciudad" value="4">Tokyo</option>
                    </optgroup>
                    <optgroup label="Europe">
                        <option name="ciudad" value="5">Amsterdam</option>
                        <option name="ciudad" value="6">London</option>
                        <option name="ciudad" value="7">Moscow</option>
                    </optgroup>
                    <optgroup label="North America">
                        <option name="ciudad" value="8">Los Angeles</option>
                        <option name="ciudad" value="9">New York</option>
                    </optgroup>
                    <optgroup label="South America">
                        <option name="ciudad" value="10">Buenos Aires</option>
                        <option name="ciudad" value="11">Sao Paulo</option>
                    </optgroup>
                </select>
                <input type="submit" value="Enviar" name="boton"/>
            </form>
            <?php
        } else {
            $ciudades = array (
                "",
                "Delhi",
                "Hong Kong",
                "Mumbai",
                "Tokyo",
                "Amsterdam",
                "London",
                "Moscow",
                "Los Angeles",
                "New York",
                "Buenos Aires",
                "Sao Paulo"                
            );
            echo "La ciudad seleccionada es ";
            echo $ciudades[$_REQUEST['ciudad']];
       
            //var_dump($ciudades);
        }
        ?>
    </body>
</html>