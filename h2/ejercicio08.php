--8
<!-- arreglar el siguiente codigo para poder seleccionar varios elementos y que nos muestre los seleccionados -->
<?php
if($_REQUEST){
    $mal=false;
} else {
    $mal=true;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"
        <title></title>
    </head>
    <body>
        <?php
        if($mal){
            ?>
            <form name="f">
                <label for="ciudad">Selecciona la ciudad</label>
                <select multiple name="ciudad" id="ciudad">
                    <optgroup label="Asia">
                        <option name="ciudad" value="3">Delhi</option>
                        <option name="ciudad" value="4">Hong Kong</option>
                        <option name="ciudad" value="8">Mumbai</option>
                        <option name="ciudad" value="11">Tokyo</option>
                    </optgroup>
                    <optgroup label="Europe">
                        <option name="ciudad" value="1">Amsterdam</option>
                        <option name="ciudad" value="5">London</option>
                        <option name="ciudad" value="7">Moscow</option>
                    </optgroup>
                    <optgroup label="North America">
                        <option name="ciudad" value="6">Los Angeles</option>
                        <option name="ciudad" value="9">New York</option>
                    </optgroup>
                    <optgroup label="South America">
                        <option name="ciudad" value="2">Buenos Aires</option>
                        <option name="ciudad" value="10">Sao Paulo</option>
                    </optgroup>
                </select>
                <input type="submit" value="Enviar" name="boton"/>
            </form>
            <?php
        } else {
            $ciudades = array (
                "",
                "Amsterdam",
                "Buenos Aires",
                "Delhi",
                "Hong Kong",
                "London",
                "Los Angeles",
                "Moscow",
                "Mumbai",
                "New York",
                "Sao Paulo",
                "Tokyo"                
            );
           
            echo "La ciudad seleccionada es ";
            echo $ciudades[$_REQUEST['ciudad']];
           
            $ciudades2=$_REQUEST['ciudad'];

            for ($i=0; $i<count($ciudades2); $i++){
                echo "<br>" . $i . ": " . $ciudades2[$i];    
            }
        }
        ?>
    </body>
</html>