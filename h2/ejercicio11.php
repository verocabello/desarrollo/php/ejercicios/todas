<!-- realizar el siguiente formulario. no son necesarias validaciones, solamente debe indicar los elementos seleccionados -->
<?php
if($_REQUEST){
    $mal=false;
} else {
    $mal=true;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"
        <title></title>
    </head>
    <body>
        <?php
        if($mal){
            ?>
            <form name="f">
                <label for="nombre">Nombre Completo</label>
                <input type="text" id="nombre" name="nombre"/><br>
                <hr>
                <label for="direccion">Dirección</label>
                <textarea name="textarea" rows="8">d</textarea><br>
                <hr>
                <label for="email">Correo Electrónico</label>
                <input type="email" id="email" name="email"/><br>
                <hr>
                <label for="clave">Contraseña</label>
                <input type="password" id="clave" name="clave"/><br>
                <hr>
                <label for="clave">Confirmar Contraseña</label>
                <input type="password" id="clave2" name="clave2"/><br>
                <hr>
                <label for="nacimiento">Fecha de Nacimiento</label>
                <input type="date" id="nacimiento" name="nacimiento"><br>
                <hr>
                <label for="sexo">Sexo</label>
                <input type="radio" id="male" name="sexo" value="Hombre">
                <label for="hombre">Hombre</label>
                <input type="radio" id="female" name="sexo" value="Mujer">
                <label for="mujer">Mujer</label><br>
                <hr>
                
                <label for="aficiones">Selecciona tus aficiones</label>
                <select id="aficiones" name="aficiones" multiple>
                    <option value="airelibre">Deportes al aire libre</option>
                    <option value="aventuras">Deportes de aventuras</option>
                    <option value="pop">Musica Pop</option>
                    <option value="rock">Musica Rock</option>
                    <option value="alternativa">Musica alternativa</option>
                    <option value="foto">Fotografía</option>
                </select>
                <br><br>
                <input type="submit" id="enviar" name="enviar"/>
            </form>
            <?php
        } else {
            $ciudades = array (
                "",
                "Amsterdam",
                "Buenos Aires",
                "Delhi",
                "Hong Kong",
                "London",
                "Los Angeles",
                "Moscow",
                "Mumbai",
                "New York",
                "Sao Paulo",
                "Tokyo"                
            );
           
            echo "La ciudad seleccionada es <br>";
            
            foreach ($_REQUEST['ciudad'] as $k=>$v){
                echo $v . "-" . $ciudades[$v];
                echo "<br>";
            }
        }
        ?>
    </body>
</html>