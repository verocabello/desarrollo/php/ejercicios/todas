<!-- arreglar el siguiente codigo para poder seleccionar varios elementos y que nos muestre los seleccionados -->
<?php
if($_REQUEST){
    $mal=false;
} else {
    $mal=true;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"
        <title></title>
    </head>
    <body>
        <?php
        if($mal){
            ?>
            <form name="f">
                <label for="ciudad">Selecciona la ciudad</label>
                <select multiple name="ciudad[]" id="ciudad">
                    <optgroup label="Asia">
                        <option value="3">Delhi</option>
                        <option value="4">Hong Kong</option>
                        <option value="8">Mumbai</option>
                        <option value="11">Tokyo</option>
                    </optgroup>
                    <optgroup label="Europe">
                        <option value="1">Amsterdam</option>
                        <option value="5">London</option>
                        <option value="7">Moscow</option>
                    </optgroup>
                    <optgroup label="North America">
                        <option value="6">Los Angeles</option>
                        <option value="9">New York</option>
                    </optgroup>
                    <optgroup label="South America">
                        <option value="2">Buenos Aires</option>
                        <option value="10">Sao Paulo</option>
                    </optgroup>
                </select>
                <input type="submit" value="Enviar" name="boton"/>
            </form>
            <?php
        } else {
            $ciudades = array (
                "",
                "Amsterdam",
                "Buenos Aires",
                "Delhi",
                "Hong Kong",
                "London",
                "Los Angeles",
                "Moscow",
                "Mumbai",
                "New York",
                "Sao Paulo",
                "Tokyo"                
            );
           
            echo "La ciudad seleccionada es <br>";
            //$ciudades=[$_REQUEST['ciudad']];
            
            foreach ($_REQUEST['ciudad'] as $k=>$v){
                echo $v . "-" . $ciudades[$v];
                echo "<br>";
            }

//            for ($i=0; $i<count($ciudades); $i++){
//                echo "<br>" . $i . ": " . $ciudades[$i];    
//            }
        }
        ?>
    </body>
</html>