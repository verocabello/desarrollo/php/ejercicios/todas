<?php
//realizar un programa que nos permita calcular la diferencia entre dos fechas introducidas.
//el programa debe cumplir las siguientes características:
// - saldrá inicialmente un formulario en pantalla centrado y con el formato indicado
// - si se carga en Chrome saldrá un calendario
// - en caso de dejar alguna de las dos cajas de texto vacias debe darnos un error
// - si se escribe alguna fecha con formato incorrecto dará error y mostrará algo
// - aunque muestro los errores recupera los datos en los controles (para no tenerlos que volver a escribir)
// - si nos colocamos encima de alguna de las dos labels debe mostrar texto de ayuda
// - otro error que se puede producir es que la fecha de fin sea inferior a la de inicio
// - el resultado debe mostrarlo utilizando una función denominada mostrar_resultado
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 8</title>
    </head>
    <style>
        *{
            margin: 0px;
            padding: 0px;
        }
        
        .errores{
            width: 200px;
            min-height: 100px;
            margin: 10px auto;
            border: 1px solid crimson;
            color: tomato;
        }
        
        .salida{
            width: 200px;
            height: 100px;
            margin: 10px auto;
            border: 1px solid #CCC;
            color: #33ccff;
            font-size: 5em;
            text-align: center;
            line-height: 100px;
        }
        
        form{
            width: 500px;
            margin: 50px auto;
        }
        
        label:hover::after{
            content: "-Ten cuidado con los formatos-";
            color: #3300ff;
        }
        
        input{
            display: block;
            margin-bottom: 10px;
        }
    </style>
    <body>
        <?php
        function mostrar_formulario($errores, $inicio = "", $fin = ""){
            if(count($errores)){
                echo '<div class="errores">';
                foreach ($errores as $error){
                    echo "$error <br/>";
                }
                echo '</div>';
            }
            ?>
        <form method="get">
            <label for="fechai">Introduce Fecha Inicial</label>
            <input type="date" id="fechai" name="fechai" required="true" placeholder="dd/mm/aaaa" 
                   value="<?php echo $inicio; ?>"/>
            <label for="fechaf">Introduce Fecha inicial Final</label>
            <input type="date" id="fechaf" name="fechaf" required="true" placeholder="dd/mm/aaaa" 
                   value="<?php echo $fin; ?>"/>
            <input type="submit" value="Calcular"/>
        </form>
        <?php
        }
        
        function mostrar_resultado($d){
            echo '<div class="salida">';
            echo $d;
            echo '</div>';
        }
        ?>
        
        <?php
        $errores = array();
        if($_REQUEST){
            $inicio = $_REQUEST["fechai"];
            $fin = $_REQUEST["fechaf"];
            
            $inicioArray = explode("/", $inicio);
            if (!@checkdate($inicioArray[1], $inicioArray[0], $inicioArray[2])) {
                $errores[] = "La fecha de inicio no es correcta";
            }
            if (!@checkdate($finArray[1], $finArray[0], $finArray[2])){
                $errores[] = "La fecha de fin no es correcta";
            }
            if (!count($errores)){
                $i = strtotime(implode("/", array_reverse($inicioArray)));
                $f = strtorime(implode("/", array_reverse($finArray)));
                if ($i <= $f){
                    $d = floor(abs(($f-$i)/(24*60*60)));
                    mostrar_resultado($d);
                }else{
                    $errores[] = "La fecha de fin debe ser mayor que la de inicio";
                }
            }
            mostrar_formulario($errores, $inicio, $fin);
        }else{
            mostrar_formulario($errores);
        }
        ?>
    </body>
</html>

