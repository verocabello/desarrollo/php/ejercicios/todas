<?php
//realizar un ejercicio que genere 100 fechas de forma aleatoria. crear la fecha en timestamp (minimo
//1/1/2000 y máximo hoy). la fecha debe mostrarse dd-mm-YYYY
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 5</title>
    </head>
    <body>
        <?php
        $maximo = time();
        $minimo = mktime(0,0,0,1,1,2000);
        
        for($c=0; $c<100; $c++){
            echo date("d-m-Y", rand($minimo,$maximo)) . "<br/>";
        }
        
        ?>
    </body>
</html>

