<?php
//indicar la salida del código
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 7</title>
    </head>
    <body>
        <?php
        function mostrar(){
            foreach(func_get_args() as $valor){
                echo "<br/>$valor<br/>";
            }
        }
        //analicemos la funcion strtotime()
        
        $fecha = "2000/12/25";
        
        mostrar(date("d/m/y", strtotime($fecha)));
        mostrar(date("d/m/y", strtotime("now")));
        mostrar(date("d/m/y", strtotime("+1 day")));
        mostrar(date("d/m/y", strtotime("+1 day", strtotime($fecha))));
        mostrar(date("d/m/y", strtotime("previous Monday")));
        
        $fecha = "10/12/2012";
        $fecha = explode("/", $fecha);
        $fecha = implode("/", array_reverse($fecha));
        mostrar(date("d/m/Y", strtotime($fecha)));
        ?>
    </body>
</html>

