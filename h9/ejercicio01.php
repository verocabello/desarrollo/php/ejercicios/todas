<?php
//indicar salida del código
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        function mostrar(){
            static $c=0;
            foreach(func_get_args() as $valor){
                echo "<br/>$c-$valor<br/>";
                $c++;
            }
        }
        //analicemos las funciones date() y time()
        
        $fechaActual = time();
        
        mostrar("Raro, raro: ",$fechaActual);
        mostrar(date("d/m/Y",$fechaActual));
        
        mostrar(date("d/m/y"));
        mostrar(date("j"),date("d"),date("D"));
        mostrar(date("N"),date("F"),date("m"));
        mostrar(date("Y"),date("y"));
        
        ?>
    </body>
</html>

