<?php
//indicar la salida del código
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 3</title>
    </head>
    <body>
        <?php
        function mostrar(){
            foreach(func_get_args() as $valor){
                echo "<br/>$valor<br/>";
            }
        }
        //analicemos las funcion es getdate() y localtime()
        
        $fecha = mktime(0,0,0,5,1,2010);
        var_dump(getdate());
        var_dump(getdate($fecha));
        var_dump(localtime());
        var_dump(localtime(time(),TRUE));
        ?>
    </body>
</html>

