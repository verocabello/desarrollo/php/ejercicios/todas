<?php
//indicar la salida del código
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 4</title>
    </head>
    <body>
        <?php
        function mostrar(){
            foreach(func_get_args() as $valor){
                echo "<br/>$valor<br/>";
            }
        }
        
        setlocale(LC_ALL, 'spanish');
        mostrar(strftime("%A/%B"));
        mostrar(strftime("%A",mktime(0,0,0,1,1,1901)));
        ?>
    </body>
</html>

