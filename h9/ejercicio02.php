<?php
//indicar la salida del código
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 2</title>
    </head>
    <body>
        <?php
        function mostrar(){
            foreach(func_get_args() as $valor){
                echo "<br/>$valor<br/>";
            }
        }
        //analicemos las funcion es mktime() y checkdate()
        
        $dia = 24;
        $mes = 1;
        $year = 2004;
        $fecha = mktime(0,0,0,$mes,$dia,$year);
        mostrar(date("d/m/y",$fecha));
        
        $dia = 35;
        $mes = 1;
        $year = 2004;
        if(checkdate($mes,$dia,$year)){
            $fecha = mktime(0,0,0,$mes,$dia,$year);
        }else{
            $fecha = mktime(0,0,0,12,1,2000);
        }
        
        mostrar(date("d/m/y",$fecha));
        
        ?>
    </body>
</html>

