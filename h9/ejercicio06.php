<?php
//crear una variable que contenga la fecha de su próximo cumpleaños en formato timestamp. debe mostrar:
// - dias que faltan
// - meses que faltan
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 6</title>
    </head>
    <body>
        <?php
        $miCumple = mktime(0,0,0,30,5,1986);
        $hoy = time();
        $proximoCumple = mktime(0,0,0,date("m",$miCumple),date("d",$miCumple),date("y",$hoy));
        
        if($proximoCumple<$hoy){
            $proximoCumple = mktime(0,0,0,date("m",$miCumple),date("d",$miCumple),date("y",$hoy)+1);
        }
        
        $segundosDia = 60*60*24;
        $dias = ($proximoCumple-$hoy)/$segundosDia;
        echo "el número de días que faltan son: " . (int)($dias) . "<br/>";
        $meses = $dias/30;
        echo "el número de meses que faltan, aproximadamente, son: " . (int)($meses) . "<br/>";
        
        $hoy = getdate();
        $proximoCumple = getdate($proximoCumple);
        
        $d["mon"] = $proximoCumple["mon"]-$hoy["mon"];
        echo "el número de meses que faltan, aproximadamente, son: " . $d["mon"];
        
        ?>
    </body>
</html>

