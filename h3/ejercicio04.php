<!-- modificar el ejercicio anterior para que acepte un segundo parametro opcional. este parametro
    si es true coloca la # al principio del color, si es false no colocara la #. por defecto es true-->
<?php
function generaColores($numero,$almohadilla){
    $colores = array();
    for($n=0; $n<$numero; $n++){
        $c=0;
        $limite=6;
        $colores[$n]="";
        if($almohadilla){
            $colores[$n]="#";
            $limite=7;
        }
        for($c=0; $c<$limite; $c++){
            $colores[$n].= dechex(mt_rand(0, 15));
        }
    }
    return $colores;
}

$almohadilla=true;
$salida= generaColores(5,$almohadilla);
var_dump($salida);
?>

