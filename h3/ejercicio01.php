<!-- desarrollar una funcion que acepte como parametros valor minimo, valor maximo y numero de valores.
    la funcion debe generar un vector con numeros aleatorios, que devolvera, ordenado de forma ascendente-->

<?php

/**
 * esta funcion genera numeros aleatorios
 * @param int $minimo valor minimo
 * @param int $maximo valor maximo
 * @param int $numero numero de valores a generar
 * @return int[] los numeros solicitados
 */

function generaNumeros($minimo,$maximo,$numero){
    //array para colocar el resultadao
    $resultado=array();
    //bucle ara rellenar el array
    for($c=0; $c<$numero; $c++){
        $resultado[$c]=mt_rand($minimo,$maximo);
    }
    //devolver el array
    return $resultado;
}

$salida = generaNumeros(1,10,10);
var_dump($salida);
?>