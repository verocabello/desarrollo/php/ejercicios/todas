<!-- crear una funcion que te devuelva en un array los ficheros existentes en un directorio ordenados 
    alfabeticamente. la funcion debe tener las siguientes caracteristicas:
        - algumento de entrada, string con ruta al directorio a analizar, por defecto directorio actual
        - salida, un array con los ficheros ordenados alfabeticamente -->
<?php
function leerDirectorio($handle = ".") {
    $handle=opendir($handle);
    while (false !== ($archivo = readdir($handle))) {
        $archivos[] = strtolower($archivo);
    }
    closedir($handle);
    sort($archivos);
    return $archivos;
}

$salida = leerDirectorio();
var_dump($salida);
?>
