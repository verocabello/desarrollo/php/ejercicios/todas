<!-- desarrollar una funcion que nos permita crear un color aleatorio en RGB. a la funcion le debemos pasar
    el numero de colores a generar y devolverá un array con esos colores-->
<?php
function generaColores($numero){
    $colores = array();
    for($n=0; $n<$numero; $n++){
        $colores[$n]="#";
        for($c=1; $c<7; $c++){
            $colores[$n].= dechex(mt_rand(0,15));
        }
    }
    return $colores;
}

$salida= generaColores(5);
var_dump($salida);
?>

