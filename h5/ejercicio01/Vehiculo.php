<?php
class Vehiculo {
    
    public $matricula;
    private $color;
    protected $encendido;
    
    public function encender(){
        $this->encendido = true;
        echo 'Vehículo encendido <br/>';
    }
    
    public function apagar(){
        $this->encendido = false;
        echo 'Vehículo apagado <br/>';
    }
}
?>
