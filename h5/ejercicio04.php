<?php
//realizar una clase que nos permita analizar las cadenas de caracteres. debe permitirnos:
// - longitud
// - numero de vocales
// - introduces una vocal y te indica cuantas veces se repite

//cadena
//-$valor
//-$longitud
//-$vocales
//=__construct($valor)
//+getValor($minusculas=FALSE)
//+getLongitud()
//+getVocales()
//+setValor($valor)
//+setLongitud($longitud)
//+setVocales($vocales)
//-calcularLongitud()
//-numeroVocales()
//+repeticionVocal($vocal)

class Cadena {
    
    private $valor;
    private $longitud;
    private $vocales;
    
    function __construct($valor){
        $this->setValor($valor);
    }
    
    public function getValor($minusculas = FALSE){
        if ($minusculas){
            return strtolower($this->valor);
        } else{
            return $this->valor;
        }
    }
    
    public function getLongitud(){
        $this->calcularLongitud();
        return $this->longitud;
    }
    
    public function getVocales(){
        $this->numeroVocales();
        return $this->vocales;
    }
    
    public function setValor($valor){
        $this->valor = $valor;
    }
    
    public function setLongitud($longitud){
        $this->longitud = $longitud;
    }
    
    public function setVocales($vocales){
        $this->vocales = $vocales;
    }
    
    private function calcularLongitud(){
        $this->setLongitud(strlen($this->valor));
    }
    
    private function numeroVocales(){
        $vocales = [
            'a',
            'e',
            'i',
            'o',
            'u'
        ];
        $longitud = 0;
        
        foreach ($vocales as $valor){
            $longitud += substr_count($this->getValor(1), $valor);
        }
        $this->setVocales($longitud);
    }
    
    public function repeticionVocal($vocal){
        $longitud = 0;
        $longitud += substr_count($this->getValor(1), $vocal);
        
        return $longitud;
    }
}

$salida = new Cadena("casa");
var_dump($salida);

?>

