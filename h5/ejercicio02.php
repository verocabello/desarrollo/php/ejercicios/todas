<?php
//indicar la salida del codigo
function __autoload($nombre_clase){
    include 'ejercicio02/' . $nombre_clase . '.php';
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 2</title>
    </head>
    <body>
        <?php
        $padre = new Persona("mario", "cob", 48);
        $hijo = $padre;
        $hija = clone $padre;
        $hijo->setEdad(100);
        $hija->setEdad(50);
        var_dump($hijo);
        var_dump($padre);
        var_dump($hija);
        ?>
    </body>
</html>

