<?php
//indicar la salida del codigo
function __autoload($nombre_clase){
    include "ejercicio01/" . $nombre_clase . '.php';
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        $camion = new Camion();
        $camion->encender();
        $camion->cargar(10);
        $camion->verificar_encendido();
        $camion->matricula = 'FGT - 3333';
        $camion->apagar();
        $autobus = new Autobus();
        $autobus->encender();
        $autobus->subir_pasajeros(5);
        $autobus->verificar_encendido();
        $autobus->matricula = 'RTH - 4444';
        $autobus->apagar();
        ?>
    </body>
</html>

